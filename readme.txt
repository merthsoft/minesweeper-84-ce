Minesweeper 84+CE v1.2
2016 Merthsoft Creations
shaun@shaunmcfall.com

Minesweeper for the TI-84+CE. See the in-game help for details.

Requires the CE libraries made by MateoConLechuga:
https://github.com/CE-Programming/libraries/releases/

Source code:
The source is maintained in a git repository available here: https://bitbucket.org/merthsoft/minesweeper-84-ce

Credits:
Many thanks to Mateo for the CE toolchain, libraries, and CEmu, all of which made this possible. He also fixed
many bugs for me I found along the way.

Thanks to Tari for all his programming help. I'm bad at C, and he is significantly better :)

Andrew Milne created the art assets, and Kerm modified some sprites to make them look a little better on-screen.

Revision History:

v1.2  2016.04.05 - Better on-calc controls

v1.1  2016.04.04 - Fix memory leak

v1.0  2016.04.03 - First full release

v0.91 2016.04.02 - Beta Release
                   Fixed helptext issues
                   New 2 and 3 sprites from kerm

v0.9  2016.04.02 - Beta Release
