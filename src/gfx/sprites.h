// Converted using ConvPNG
// This file contains all the graphics for easier inclusion in a project
#ifndef sprites_H
#define sprites_H
#include <stdint.h>

extern uint8_t sprites_transpcolor_index;
extern uint8_t zero[400];
#define zero_width 20
#define zero_height 20
extern uint8_t one[400];
#define one_width 20
#define one_height 20
extern uint8_t two[400];
#define two_width 20
#define two_height 20
extern uint8_t three[400];
#define three_width 20
#define three_height 20
extern uint8_t four[400];
#define four_width 20
#define four_height 20
extern uint8_t five[400];
#define five_width 20
#define five_height 20
extern uint8_t six[400];
#define six_width 20
#define six_height 20
extern uint8_t seven[400];
#define seven_width 20
#define seven_height 20
extern uint8_t eight[400];
#define eight_width 20
#define eight_height 20
extern uint8_t explode[400];
#define explode_width 20
#define explode_height 20
extern uint8_t filled[400];
#define filled_width 20
#define filled_height 20
extern uint8_t flagged[400];
#define flagged_width 20
#define flagged_height 20
extern uint8_t mine[400];
#define mine_width 20
#define mine_height 20
extern uint8_t misflagged[400];
#define misflagged_width 20
#define misflagged_height 20
extern uint8_t question[400];
#define question_width 20
#define question_height 20
extern uint8_t win[400];
#define win_width 20
#define win_height 20
extern uint8_t cursor[400];
#define cursor_width 20
#define cursor_height 20
extern uint8_t logo1[5250];
#define logo1_width 210
#define logo1_height 25
extern uint8_t logo2[1880];
#define logo2_width 94
#define logo2_height 20
extern uint16_t sprites_pal[58];

#endif
